import subprocess
import sys
import os
from os import path


def Install(packages):
    command = [sys.executable, "-m", "pip", "install"]
    command.append(packages)
    FNULL = open(os.devnull, 'w')
    subprocess.call(command, stdout=FNULL, stderr=subprocess.STDOUT)

def GetParam(envName, inputPrompt, inputLambda):
    val = os.environ.get(envName)
    if val is None:
        val = inputLambda(inputPrompt)
    return val


def Main():

    print("** MANGA DOWNLOADER **")

    print("> Installing Manga-Py")
    Install("manga-py")

    
    print("> Settings")
    comicURL = GetParam("COMIC_URL", "URL: ", input)
    comicReverse = GetParam("COMIC_LATEST_FIRST", "Download latest first? (Y/N): ", input)
    comicMaxChapters = GetParam("COMIC_MAX", "Max chapters to download (all):", input)


    print("> Downloading")
    command = ["manga-py", comicURL, "-R", "--show-current-chapter-info"]
    if comicReverse == "y" or comicReverse == "Y":
        command.append("--reverse-downloading")

    if comicMaxChapters != "" and comicMaxChapters != "all":
        command.append("--max-volumes")
        command.append(comicMaxChapters)

    subprocess.call(command)
    return 0

if __name__ == '__main__':
    sys.exit(Main())
