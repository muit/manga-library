import os
import sys
import glob
import subprocess
from os import path
from pathlib import Path
import shutil
import zipfile
import warnings


def Install(packages):
    command = [sys.executable, "-m", "pip", "install"]
    command.append(packages)
    FNULL = open(os.devnull, 'w')
    subprocess.call(command, stdout=FNULL, stderr=subprocess.STDOUT)

def GetParam(envName, inputPrompt, inputLambda):
    val = os.environ.get(envName)
    if val is None:
        val = inputLambda(inputPrompt)
    return val

class Comic(object):
    pass
class Volume(object):
    pass
class Chapter(object):
    pass

class TempDir(object):
    def __init__(self):
        return

    def __enter__(self):
        self.path = os.path.join(os.getcwd(), "Temp")
        if os.path.exists(self.path):
            shutil.rmtree(self.path)
        os.mkdir(self.path)
        return self

    def __exit__(self, type, value, traceback):
        shutil.rmtree(self.path)
        return

    def PathTo(self, name):
        return os.path.join(self.path, name)


kindleGenPath = path.join(os.getcwd(), "bin", sys.platform)
kindleGenFile = path.join(kindleGenPath, "kindlegen")
if "win" in sys.platform:
    kindleGenPath = path.join(os.getcwd(), "bin", "win")
    kindleGenFile = path.join(kindleGenPath, "kindlegen.exe")

def Main():
    Install("KindleComicConverter")

    if not path.isfile(kindleGenFile):
        print("Couldnt find KindleGen inside Bin.")
        return -1

    comic = Comic()
    comic.title  = GetParam("COMIC_TITLE",  "Title: ",      input)
    comic.author = GetParam("COMIC_AUTHOR", "Author: ",     input)
    comic.path   = GetParam("COMIC_PATH",   "Images folder: ", input)
    if comic.title is None:
        print("A title is required")
        return -1
    if comic.path is None or not path.isdir(comic.path):
        print("Invalid comic path")
        return -1

    maxChapters = GetParam("COMIC_MAX_CHAPTERS", "Max chapters per book (8): ", input)
    if maxChapters is None or maxChapters == "":
        maxChapters = 8
    else:
        maxChapters = int(maxChapters)
    
    chapters = FindChapters(comic)
    chaptersNum = len(chapters)

    print("> Generating EBooks for %i chapters" % chaptersNum)
    with TempDir() as tempDir:

        firstChapter = 0
        lastChapter = maxChapters - 1
        
        while firstChapter < chaptersNum:
            CreateVolumeBook(chapters, firstChapter, lastChapter, tempDir, comic)

            firstChapter = lastChapter + 1
            lastChapter += maxChapters
            if lastChapter >= chaptersNum:
                lastChapter = chaptersNum - 1

def FindChapters(comic):
    chapters = []
    chapterFiles = glob.glob(os.path.join(comic.path, '*.zip'))
    for chapterFile in chapterFiles:
        if ".IMAGES_SKIP_ERROR" in chapterFile:
            warnings.warn("Skipped chapter due to missing images. %s" % chapterFile)
            continue

        chapterPath = Path(chapterFile)

        idSections = []
        stemSections = chapterPath.stem.split('-')
        for section in stemSections:
            if section.startswith('vol_'):
                idSections.append(section.strip("vol_"))
            elif section.startswith('ch_'):
                idSections.append(section.strip("ch_"))

        try:
            lastSection = stemSections[len(stemSections)-1]
            if int(lastSection) > 0:
                idSections.append(lastSection)
        except ValueError:
            pass

        chapter = Chapter()
        chapter.id = ".".join(idSections)
        chapter.file = chapterFile
        chapters.append(chapter)
    return chapters

def CreateVolumeBook(chapters, first, last, tempDir, comic):
    firstId = chapters[first].id
    lastId = chapters[last].id

    volume = Volume()
    volume.id=firstId + "-" + lastId
    volume.title = comic.title + " " + volume.id
    volume.path = tempDir.PathTo(volume.title)

    outputPath = path.join(os.getcwd(), "Books", comic.title)
    finalBookFile = path.join(outputPath, volume.title + ".mobi")
    if path.isfile(finalBookFile) and path.exists(finalBookFile):
        print("Skipping volume '%s'. Found at destination." % volume.title)
        return

    print("Creating volume '%s'" % volume.title)

    os.mkdir(volume.path)
    SaveMetaData(comic, volume, tempDir)
    for i in range(first, last + 1):
        chapter = chapters[i]

        with zipfile.ZipFile(chapter.file, 'r') as zipFile:
            zipFile.extractall(path.join(volume.path, chapter.id))
    

    command = ["kcc-c2e", "--profile=KPW", "--manga-style", "--output=%s" % outputPath, "--batchsplit=0",
        "--title=%s" % volume.title,
        volume.path
    ]
    try:
        subprocess.call(command, cwd=kindleGenPath)
    except KeyboardInterrupt as e:
        sys.exit(1)
    except:
        print("Volume generation failed")

def GetExtractPath(chapter, tempDir):
    return path.join(tempDir, chapter.id)

def SaveMetaData(comic, volume, tempDir):
    f = open(path.join(volume.path, "ComicInfo.xml"), "w", encoding='UTF-8')
    f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"\
    "<ComicInfo xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"\
        "<Series>%(title)s</Series>\n"\
        "<Writer>%(author)s</Writer>\n"\
    "</ComicInfo>" % dict(
        title=comic.title,
        author=comic.author
    ))
    f.close()

if __name__ == '__main__':
    sys.exit(Main())
